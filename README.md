# README #

Trabajo de SAD(Sistemas de apoyo a la decisión).

### ¿Que hace este repositorio? ###

* Lee un fichero .arf y le aplica un filtro, posteriormente saca los resultados por pantalla.

### ¿Como funciona este repositorio? ###

* Este programa lee un archivo .arf(archivo de weka).
* Le aplica el filtro STWV(String To Word Vector).
* Graba los datos en un nuevo fichero .arf.

### Tecnología  ###

* Java
* API de Weka(http://www.cs.waikato.ac.nz/ml/weka/)

### Creador ###

* Jonathan Guijarro Garcia