package trabajo_sad;


	/*
	 GOAL: Load data from .arff files, preprocess the data, train a model and assess it either using 10-fold cross-validation or hold-out
	 
	 Compile:
	 javac DataMiningExample.java

	 Run Interpret:
	 java DataMiningExample
	 
	 HACER!!!
		- Hacer modular
		- El programa no puede tener dependencias con datos!
		- Generar un .jar y ejecutar desde la l�nea de comandos

	 */


import weka.core.Instances;


	public class Principal {
		
	    public static void main(String[] args) throws Exception {
			
	    	
	    	/////////////Lectura de datos y aplica el filtro SWTV/////////////
	    	Lectura lect= new Lectura();
	    	Instances dataSel;
	    	dataSel = lect.cargarDatos(args[0]);
	    	
	
	    	/////////////Creamos el arff/////////////
	    	Escribir esc= new Escribir();
	    	esc.escribir(dataSel,args[1]);
	    }
}


