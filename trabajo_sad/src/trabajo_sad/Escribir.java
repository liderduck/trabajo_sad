package trabajo_sad;

import java.io.FileWriter;
import java.io.PrintWriter;

import weka.core.Instances;


public class Escribir {
	
	public void escribir(Instances inst,String args) {
		FileWriter fichero = null;
		PrintWriter pw = null;
		try {
			int z = 0;
			int j=0;
			fichero = new FileWriter(args);
			pw = new PrintWriter(fichero);
			int ultimo = inst.numInstances();
			int ultimoAtr=inst.numAttributes();
			pw.println(inst.relationName());
			while(j!=ultimoAtr){
				pw.println(inst.attribute(j));
				j++;
			}
			pw.println("@data");
			while (z != ultimo) {
				pw.print(inst.instance(z));
				pw.println();
				z++;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != fichero)
					System.out.println("Archivo "+ args + " creado");
					fichero.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
	
}
